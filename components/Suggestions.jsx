import React from "react";
import { GiftIcon, CashIcon, HeartIcon } from "@heroicons/react/solid";
import CopyToClipboard from "./CopyToClipboard";

const options = [
  {
    title: "Nua",
    icon: <GiftIcon className="w-8 h-8" />,
    description: (
      <>
        <ul className="space-y-2 list-disc">
          <li>
            Código de lista de bebé:{" "}
            <span className="italic font-bold">5499</span>
          </li>
          <li>
            Entra a{" "}
            <a
              href="https://consultas931.wixsite.com/listadebabyshower/listas-de-baby-shower"
              target="_blank"
              rel="noreferrer"
            >
              https://consultas931.wixsite.com/listadebabyshower/listas-de-baby-shower
            </a> y busca el código
          </li>
          <li>
            Encontrarás la lista de sugerencias que hemos creado, escoge el
            presente que más te guste y visualizarlo en la página:{" "}
            <a
              href="https://nua.com.pe/"
              target="_blank"
              rel="noreferrer"
            >
              https://nua.com.pe/
            </a>
          </li>
          <li>
            Realiza la compra vía WhatsApp, consultando el stock:{" "}
            <a
              href="http://wppredirect.tk/go/?p=51932009194&m=Hola,%20necesito%20informaci%C3%B3n%20de%20%0A"
              target="_blank"
              rel="noreferrer"
            >
              WhatsApp
            </a>
          </li>
          <li>
            Si aún tienes dudas, podrán realizar el pedido a través de Facebook (Nuababy), Instagram(nuababyperu),  whatsapp 960 045 901 o al correo listadebebes@nua.com.pe indicando nuestro nombre o el código de tu lista y código del producto.
          </li>
          <li>
            Si gustas enviar un presente, que no este en la lista, a nuestro domicilio. Usa estos datos:
            <p><span className="font-bold">Nombres:</span> Sylvia Ramos</p>
            <p><span className="font-bold">Dirección:</span> Calle Colcabamba 240, Los Olivos, Lima</p>
            <p><span className="font-bold">DNI:</span> 43069588</p>
          </li>
          
        </ul>
      </>
    ),
  },
  {
    title: "Recuerda",
    icon: <HeartIcon className="w-8 h-8" />,
    description: (
      <>
        Cualquier otro detalle de tu parte, siempre será bien recibido con mucho
        cariño.
      </>
    ),
  },
];

const Suggestions = () => {
  return (
    <div className="flex flex-col gap-4">
      {options.map((option) => {
        return (
          <div
            className="flex py-4 pl-3 pr-12 bg-white rounded-md shadow-md"
            key={option.title}
          >
            <div className="mr-4 text-primary">{option.icon}</div>
            <div>
              <h4 className="mb-2 text-2xl text-primary">{option.title}</h4>
              <div className="leading-5 text-gray-500">
                {option.description}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Suggestions;
