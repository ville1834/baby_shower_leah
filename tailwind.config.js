module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      body: "Lora, serif",
      sans: "Roboto Condensed, sans-serif",
      mono: "licorice, cursive",
    },
    extend: {
      colors: {
        primary: "#9FD192",
        secondary: "#F4CCCC",
        tertiary: "#F1EEFA",
        quaternary: "#b4a7d6",
        brown: "#FFD966",
      },
    },
  },
  plugins: [],
};
